#!/bin/bash

count=0
bspc subscribe node_add | while read event monitor_id desktop_id focus_id node_id; do
	window_info="$(xprop -id "$node_id")"
	if [[ "$(echo "$window_info" | grep 'listen.tidal.com')" ]] || [[ "$(echo "$window_info" | grep 'app.plex.tv')" ]] || [[ "$(echo "$window_info" | grep 'Jellyfin')" ]]; then
		bspc desktop -f ^1
		bspc node -f "$node_id"
		bspc node -d ^2
		count="$(( "$count" + 1 ))"
	else
		if [[ "$(echo "$window_info" | grep 'Vivaldi-snapshot\|kotatogram-desktop')" ]]; then
			bspc desktop -f ^1
			bspc node -f "$node_id"
			bspc node -d ^1
			count="$(( "$count" + 1 ))"
		fi
	fi
	if [[ $count == 5 ]]; then
		bspc desktop -f ^1
		bspc node @east -r +235
		bspc node @west -r +235
		exit 0
	fi
done
