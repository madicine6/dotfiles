#!/bin/bash

term='termite -t quake'
wmclass=Termite
geometry="250 0 2060 420"
file=/tmp/quake_term

exec_quake() {
	bspc rule -a $wmclass state=floating sticky=on
	bspc subscribe node_add | while read event monitor_id desktop_id focus_id node_id; do
		echo "$node_id" > "$file"
		xprop -id "$node_id" -f _NET_WM_WINDOW_TYPE 32a -set _NET_WM_WINDOW_TYPE _NET_WM_WINDOW_TYPE_DOCK
		wtp $geometry "$node_id"
		bspc node -f "$node_id"
		bspc rule -r "$wmclass"
		break
	done &
	$term &
}

refresh() {
	wtp $geometry "$node_id"
	bspc node -f "$node_id"
}

if [[ -f $file ]]; then
	node_id="$(cat "$file")"
	if [[ $(bspc query -N | grep "$node_id") ]]; then
		if [[ $(xprop -id "$node_id" | grep 'WM_NAME(STRING) = "quake"') ]]; then
			if [[ $(bspc query -N -n .hidden | grep "$node_id") ]]; then
				bspc node "$node_id" -g hidden=off
				refresh
			else
				if [[ $(bspc query -N -d | grep "$node_id") ]]; then
					bspc node "$node_id" -g hidden=on
				else
					refresh
				fi
			fi
		else
			exec_quake
		fi
	else
		exec_quake
	fi
else
	exec_quake
fi
