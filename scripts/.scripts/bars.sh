#!/bin/bash

while [[ $count < 5 ]]; do
	count=0
	winlist="$(lsw)"
	for i in $winlist; do
		if [[ $(xprop -id "$i" | grep WM_CLASS | grep 'xfce4-panel\|polybar') ]]; then
			count="$(( $count + 1 ))"
echo $count
			chwso -l "$i"
		fi
	done
	sleep 1
done

for i in $winlist; do
	if [[ $(xprop -id "$i" | grep 'xfce4-panel') ]]; then
		chwso -l "$i"
	fi
done

for i in $winlist; do
	if [[ $(xprop -id "$i" | grep 'polybar') ]]; then
		if (( $(wattr x "$i") > $(( $(xrandr | head -n1 | sed -r 's/.*current (.*) x .*,.*/\1/') / 2 )) )); then
			chwso -l "$i"
		fi
	fi
done
