#!/bin/bash

echo '■'

playerctl -p spotify -F status | while read string; do
	case "$string" in
		'Playing')
			echo '▶'
		;;
		'Paused')
			echo '▮▮'
		;;
		*)
			echo '■'
		;;
	esac
done
