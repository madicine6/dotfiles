#!/bin/bash

pkill xfce4-panel
pkill polybar
pkill volume_notify
pkill dunst
#feh --no-fehbg --bg-fill ~/Pictures/berserk_armor4.png &
feh --no-fehbg --bg-fill ~/Pictures/offset.png > /dev/null &
dunst > /dev/null &
xfce4-panel -d > /dev/null &
polybar -c $HOME/.config/polybar/polybar left > /dev/null &
polybar -c $HOME/.config/polybar/polybar right > /dev/null &

$HOME/.scripts/volume_notify.sh &
$HOME/.scripts/bars.sh
