#!/bin/bash

while true; do
	stdbuf -oL alsactl monitor | while read; do
		volume="$(amixer sget Master | grep 'Front Right:' | awk -F'[][]' '{ print $2 }')"
		gdbus call --session --dest org.freedesktop.Notifications --object-path /org/freedesktop/Notifications --method org.freedesktop.Notifications.Notify Volume 66 '' "Volume: $volume" '' '[]' '{"urgency": <byte 1>}' 'int32 -1'
	done
done
