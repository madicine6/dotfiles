#!/bin/bash

pkill volume_notify
$HOME/.scripts/volume_notify.sh &
[[ ! "$(pgrep sshtun)" ]] && $HOME/.scripts/sshtun.sh &
