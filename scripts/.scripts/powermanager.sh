#!/bin/bash

ss_time=300000
blank_time=270000
locker="$HOME/.scripts/lock.sh"
locker_process="lock.sh"
nowplaying_apps="Chromium\|Firefox"
fl_exclude='WM_CLASS(STRING) = "vivaldi-snapshot", "Vivaldi-snapshot"\|WM_CLASS(STRING) = "Navigator", "Firefox Beta""'
pm_state="/tmp/pm_state"

xset dpms 0 0 0

for i in $nowplaying_apps; do
	pattern="$pattern\|client: .* <$i>"
done
pattern="${pattern:2}"

#fl_exclude="WM_CLASS(STRING) = $fl_exclude"

if (( $ss_time < $blank_time )); then
	first_time="$ss_time"
	second_time="$blank_time"
else
	first_time="$blank_time"
	second_time="$ss_time"
fi

echo on > "$pm_state"
while true; do
	lock_block="$(xdotool getmouselocation --shell | grep -c 'X=0\|Y=0')"
	time="$(xprintidle)"
	nowplaying_app="$(pacmd list-sink-inputs | grep -o "$pattern")"
	if [[ $(cat "$pm_state") == 'on' ]] && [[ $lock_block != 2 ]] && [[ ! $nowplaying_app ]]; then
		if (( $time > $ss_time )) && [[ ! $(pgrep "^${locker_process}$") ]]; then
			$locker &
		fi
		if (( $time > $blank_time )); then
			xset dpms force standby
		fi
	fi
	if [[ $lock_block != 2 ]] && [[ ! $nowplaying_app ]]; then
		if (( $time < $first_time )); then
			sleep_time="$(( $first_time - $time ))"
		else
			if (( $time > $second_time )); then
				sleep_time="$first_time"
			else
				sleep_time="$(( $second_time - $first_time ))"
			fi
		fi
	else
		sleep_time="$first_time"
	fi
	if (( $sleep_time > 1000 )); then
		sleep "${sleep_time:0:-3}"
	else
		sleep 1
	fi
done &

switch_state() {
	if [[ $(cat "$pm_state") != "$1" ]]; then
		echo "$1" > "$pm_state"
	fi
}

bspc subscribe node | while read event monitor_id desktop_id node_id state value; do
	case $event in
		node_state)
			if [[ $state == 'fullscreen' ]]; then
				if [[ ! $(xprop -id "$node_id" | grep "$fl_exclude") ]]; then
					if [[ $value == 'on' ]]; then
						switch_state off
					else
						switch_state on
					fi
				else
					switch_state on
				fi
			fi
		;;
		node_focus)
			if [[ $(xprop -id "$node_id" | grep '_NET_WM_STATE_FULLSCREEN') ]]; then
				if [[ ! $(xprop -id "$node_id" | grep "$fl_exclude") ]]; then
					switch_state off
				else
					switch_state on
				fi
			else
				switch_state on
			fi
		;;
	esac
done
