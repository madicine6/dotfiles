#!/bin/bash

cleanup() {
	kill $pid2
	kill $pid
#	rm "$fp"
#	rm "$fp_blurred"
}
trap cleanup EXIT

fs_feh() {
	feh -F "$fp" &
	pid="$!"
}

fp="/tmp/screen.png"
fp_blurred="/tmp/screen_blurred.png"
maim -u "$fp"
feh -F "$fp" &
pid2="$!"
fs_feh

convert "$fp" -filter Gaussian -resize 5% -define filter:sigma=2.5 -resize 2000% -attenuate 0.2 +noise Gaussian "$fp_blurred"
while true; do
	xy="$(slop)"
	if [ ! -z "$xy" ]; then
		convert "$fp" \( "$fp_blurred" -crop "$xy" \) -geometry "+$(echo $xy | cut -d "+" -f 2-3)" -composite "$fp"
		kill $pid
		fs_feh
	else
		break
	fi
done

xclip -selection c -t image/png "$fp"
