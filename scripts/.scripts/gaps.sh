#!/bin/bash

tmp="/tmp/gaps"

if [ "$(cat "$tmp")" == "on" ]; then
	bspc config window_gap 0
	echo "off" > "$tmp"
else
	bspc config window_gap 25
	echo "on" > "$tmp"
fi
