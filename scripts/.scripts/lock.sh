#!/bin/bash

set -e
function cleanup() {
	rm "$file"
}
trap cleanup EXIT

if [ ! "$(pgrep i3lock)" ]; then

file="/tmp/lock.png"
#colorbg="2F343FFF"
#colorbgtr="5294E275"
#colorhl="5294E2FF"
#colorhltr="5294E2FF"

colorbg="373737FF"
colorbgtr="BEBEBE75"
colorhl="BEBEBEFF"
colorhltr="BEBEBEFF"

colorer="BB0000FF"
colorertr="BB0000FF"

pkill '^dunst$'
setxkbmap -layout us

maim "$file"
convert "$file" -filter Gaussian -resize 5% -define filter:sigma=2.5 -resize 2000% -attenuate 0.2 +noise Gaussian "$file"
i3lock --nofork -f -i "$file" --indicator --timecolor CCCCCCFF --datecolor CCCCCCFF --insidecolor $colorbg --ringcolor $colorbgtr -s --separatorcolor 00000000 --keyhlcolor $colorhl --insidevercolor $colorhl --ringvercolor $colorhltr --insidewrongcolor $colorer --ringwrongcolor $colorertr --wrongcolor $colorhl -k --timestr "%H:%M" --datestr "%a, %b %d"

dunst &
setxkbmap -layout us,ru -variant us -option grp:caps_toggle terminate:ctrl_alt_bksp

fi
