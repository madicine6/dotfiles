#!/bin/bash

func() {
for pacin in $paclist; do
    if [[ $pac = *$pacin* && $pac != $bac ]]; then
        if [ ! -z "$inp" ]; then
            echo "$pac"
            ct="$(( $ct + 1 ))"
        else
            echo "$pac   $i" | sed "s/\$repo/x86_64/g"
        fi
        bac=$pac
    fi
done
}

[ -z "$1" ] && paclist="$(pacman -Qqm | sed 's/-git//g')" && inp=1 || paclist="$1"

for i in $(curl -L -s --connect-timeout 5 https://wiki.archlinux.org/index.php/Unofficial_user_repositories | grep "Server = http" | cut -d " " -f 3 | sed 's/\$arch/x86_64/g'); do
    if [ "$(echo $i | grep "\$repo")" ]; then
        for rep in $(curl -L -s --connect-timeout 5 "$i" | grep -oP '<a href="\K.*' | cut -d \" -f 1); do
            bac=""
            for pac in $(curl -L -s --connect-timeout 5 "$i" | sed 's/\$repo/$rep/g' | grep -oP '<a href="\K.*' | grep ".pkg.tar.xz" | grep -v ".sig" | sed 's/-[^-]\+-[^-]\+-[^-]\+.pkg.tar.xz.*//'); do
                func
            done
        done
    else
        bac=""
        for pac in $(curl -L -s --connect-timeout 5 "$i" | grep -oP '<a href="\K.*' | grep ".pkg.tar.xz" | grep -v ".sig" | sed 's/-[^-]\+-[^-]\+-[^-]\+.pkg.tar.xz.*//'); do
            func
        done
    fi
    [ ! -z "$ct" -a -z "$1" ] && echo -e "$ct package(s) found in repository: $i\\n" && ct=""
done
