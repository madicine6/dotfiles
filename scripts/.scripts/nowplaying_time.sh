#!/bin/bash

echo '00:00/00:00'

playerctl -p spotify -F metadata --format '{{duration(position)}}/{{mpris:length}}' | while read string; do
	if [[ "$string" ]]; then
		len="${string#*/}"
		len_sec="$(( $len / 1000000 ))"
		(( $len_sec > 6000 )) && format='%H:'
		string="$(echo "$string" | sed "s/$len$/$(date -d@"$len_sec" -u +"$format"%M:%S)/")"
		if [[ "${string:1:1}" == ':' ]]; then
			string=0"$string"
		fi
		echo "$string"
	else
		echo '00:00/00:00'
	fi
done
