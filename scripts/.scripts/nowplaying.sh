#!/bin/bash

max_chars=29

playerctl -p spotify -F metadata --format '{{xesam:artist}} - {{xesam:title}} [{{xesam:album}}] {{position}}' | while read string; do
	if [[ "$string" ]]; then
		string="${string% *}"
		chars="$(( ${#string} + 1 ))"
		if (( "$chars" > "$max_chars" )); then
			if [[ "$chars" == "$prev_chars" ]]; then
				offset="$(( $offset + 1 ))"
				echo "$string" | tail -c +"$offset" | head -c "$max_chars"
				echo
				if (( "$(( $offset + $max_chars ))" >= "$chars" )); then
					offset=0
				fi
			else
				prev_chars="$chars"
				offset=0
				echo "$string" | head -c "$max_chars"
				echo
			fi
		else
			echo "$string"
		fi
	else
		echo Nothing
	fi
done
