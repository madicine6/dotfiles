#!/bin/bash

start_ssh() {
	ssh root@my.media -nNT -R 80:localhost:32400 &
	pid="$!"
}	

start_ssh

while true; do
	[[ $(timeout 5 curl my.media) ]] || {
		kill "$pid"
		retval='124'
		while [[ "$retval" == '124' ]]; do
			timeout 5 ssh root@my.media 'kill -9 $(fuser 80/tcp 2>/dev/null)'
			retval="$?"
		done
		start_ssh
	}
	sleep 5
done
