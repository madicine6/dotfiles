#!/bin/bash

cookie='cookie: movie-filter=opened; _identity=89df59f56df16022ee031a9aa8c0158c41f35d8eb89e996a6a105edcc3b92e8ca%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A51%3A%22%5B376093%2C%22rxGH0jnhgJcKsjk1IMqYcc9_KfwhlIO1%22%2C7776000%5D%22%3B%7D; token=9518ab9ea5217abb30cd7f9c3c3559aa93a3f45e3209af2851f428be8b98615fa%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22token%22%3Bi%3A1%3Bs%3A64%3A%22YM6Fipa7XfoM4Msvwl7XU3FwNXxjfkf1xBih5q3SjzT6ZQ9RQH36LhEpWMewLp8e%22%3B%7D; _csrf=d7add67314362aae73b297c9e5d580a77c783a773c526a6efd87830e416a9563a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22mQ31Z-owNyVr5KFD0Lf9luwQQeG6a1t7%22%3B%7D; PHPSESSID=gue1gidc7j05f9k9ri0f7is4s8'
moviespath=~/"Videos/Movies"
seriespath=~/"Videos/Series"

item="$(echo "$*" | sed 's/[[:blank:]]/%20/g')"

content="$(curl -s "https://api.service-kp.com/v1/items/search?q=$item" \
	-H "$cookie" \
	--compressed)"

list="$(echo "$content" | jq -r '.items[] | "\(.id)"')"

number=1
for id in $list; do
	echo "$content" | jq -r '.items[] | select(.id=='"$id"') | "'"$number: "'\(.title) (\(.year))"'
	number="$(( $number + 1 ))"
done

if [[ $number == 1 ]]; then
	exit 1
fi

echo "Select:"
re='^[0-9]+$'
while ! [[ $input =~ $re ]] || (( $input >= $number )); do
	read input
done

contype="$(echo "$content" | jq -r '.items['"$(( $input - 1 ))"'].type')"
id="$(echo "$content" | jq -r '.items['"$(( $input - 1 ))"'].id')"
title="$(echo "$content" | jq -r '.items[] | select(.id=='"$id"') | "\(.title) (\(.year))"' | sed 's/.*\/ //')"

function operask() {
	echo "1: Stream"
	echo "2: Download"
	echo "3: Print"
	while ! [[ $oper =~ $re ]] || (( $oper > 3 )); do
		read oper
	done
	if [[ $oper == 1 ]]; then
		linkformat='hls'
		fileformat='m3u8'
	else
		linkformat='http'
		fileformat='mp4'
	fi
}

if [[ ! "$contype" == 'serial' ]]; then
	operask
	link="$(curl -s "https://api.service-kp.com/v1/items/$id" \
		-H "$cookie" \
		--compressed | jq -r '.item.videos[].files[0].url.'"$linkformat")"
	if [[ $oper == 3 ]]; then
		echo "$link"
	else
		aria2c -s16 -x16 "$link" -d / -o "$moviespath/${title}.$fileformat"
	fi
	#mkdir "$moviespath/${title}"
	#fstreamfs -L "$link" "$moviespath/${title}"
else
	info="$(curl -s "https://api.service-kp.com/v1/items/$id" \
		-H "$cookie" \
		--compressed)"
	smax="$(echo "$info" | jq -r '.item.seasons[-1].number')"
	echo "Number of seasons: $smax"
	echo "Select season:"
	while ! [[ $snum =~ $re ]] || (( $snum > $smax )); do
		read snum
	done
	mkdir -p "$seriespath/$title/Season $snum"
	operask
	for i2 in $(echo "$info" | jq -r '.item.seasons[] | select(.number=='"$snum"') | .episodes[].number'); do
		link="$(echo "$info" | jq -r '.item.seasons[] | select(.number=='"$snum"') | .episodes[] | select(.number=='$i2') | .files[0].url.'"$linkformat")"
		if [[ $oper == 3 ]]; then
			echo "s${snum}e${i2}:"
			echo "$link"
		else
			aria2c -s16 -x16 "$link" -d / -o "$seriespath/$title/Season $snum/s${snum}e${i2}.$fileformat"
		fi
		#mkdir "$seriespath/$title/Season $snum/s${snum}e${i2}"
		#astreamfs -L "$link" "$seriespath/$title/Season $snum/s${snum}e${i2}"
	done
fi
