#!/bin/bash

fail() {
	echo "ERROR: Unresolved conflicts"
}

args="$*"
for i in $args; do
	if [[ $i == '-f' ]]; then
		force=1
echo $args
		args="$(echo "$args" | sed 's/-f//')"
	fi
done

if [[ ! -z $args ]]; then
	for i in $args; do
		if [[ ! -d ./$i ]]; then
			echo "Directory $i doesn't exist."
			exit
		fi
	done
else
	args='*/'
fi

IFS=$'\n'
for i in $args; do
	pushd "$i" > /dev/null
	for x in $(find . -type f); do
		file="../../$x"
		echo "$file"
		if [[ $force == 1 ]]; then
			rm "$file"
		else
			if [[ ! -h $file ]] && [[ -f $file ]]; then
				echo "Regular file already exists. Rewrite it with symlink?"
				select yn in 'Yes' 'No'; do
					case $yn in
						Yes )
							rm "$file"
							break
						;;
						No )
							fail
							exit
						;;
					esac
				done
			else
				if [[ -h $file ]] && [[ ! -e $file ]]; then
					rm "$file"
				else
					if [[ -d $file ]]; then
						echo "Target is a directory. Remove?"
						select yn in 'Yes' 'No'; do
							case $yn in
								Yes )
									rm -rf "$file"
									break
								;;
								No )
									fail
									exit
								;;
							esac
						done
					fi
				fi
			fi
		fi
	done
	popd > /dev/null
done

stow $args
