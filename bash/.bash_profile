#
# ~/.bash_profile
#

echo 'Choose display:'
select disp in PC TV; do
	export disp=$disp
	break
done

if [[ -z $DISPLAY && $XDG_VTNR -le 2 ]]; then #&& ! -f "/tmp/Xlock" ]]; then
       exec startx
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc

source /home/tenshin/.config/broot/launcher/bash/br
