#
# ~/.bashrc
#

#export TERM=ansi
if [[ $- == *i* ]] && [[ -x "$(command -v fish)" ]] && [[ ! -f /tmp/kek ]]; then
	exec fish
fi

#Aliases
alias mirrors='curl -s "https://www.archlinux.org/mirrorlist/?country=BY" > /tmp/mirrorlist && chmod 755 /tmp/mirrorlist && sed -i '\''s/#//'\'' /tmp/mirrorlist && rankmirrors -n 10 /tmp/mirrorlist | sudo tee /etc/pacman.d/mirrorlist && rm /tmp/mirrorlist'
alias neofetch='clear && neofetch --ascii_colors 7 7 --colors 7 7 7 7 --color_blocks off'
alias nload='nload -u K'
alias ccache='sudo paccache -rk 2 && sudo paccache -ruk 0'
alias passgen='cat /dev/urandom | tr -cd "[:alpha:][:digit:]" | tr -d "oO0lI" | head -c8 && echo'
alias pbd="expac --timefmt='%s' '%l@%n' | sort | sed 's/.*@//' | pacman -Qe -"
alias my='ssh root@my.media'
alias bat='bat --theme=OneHalfDark'
alias xcin='startx ./.xinitrc2'
alias xkde='startx ./.xinitrc3'
alias xbsp='startx'

qdtt() { pac="$(yay -Qqdtt)"; [ -n "$pac" ] && yay -Rscn $pac; }

#Xorg
alias empty='startx ./.xinit/xempty'
alias xmin='startx ./.xinit/xmin'

alias nano='edit nano'
edit() {
if [ -f "$2" ]; then
    if [ -w "$2" ]; then
        $1 "$2"
    else
        echo -n "Warning! Editing file as root!"
        read -rn1
        sudo $1 "$2"
    fi
else
    $1 "$2"
fi
}


transfer() {
	if [ $# -eq 0 ];then
	    echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
	    return 1
	fi
	tmpfile=$( mktemp -t transferXXX )
	if tty -s; then
	    basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g')
	    curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile
	else
	    curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile
	fi
	cat $tmpfile
	rm -f $tmpfile
}

command_not_found_handle() {
	[[ $1 =~ .*.txt ]] && nano "$1" || echo 'Command not found.'
}

PS1='\[\033[1m\]\u\[\033[90;7m\] \[\033[27;100;39m\]\W \[\033[49;90m\]\[\033[0m\] '

source /etc/profile.d/vte.sh

[[ -e "/usr/share/fzf/fzf-extras.bash" ]] \
  && source /usr/share/fzf/fzf-extras.bash

if [[ $- =~ .*i.* ]]; then
	bind '"\C-r": "\C-a fhe \C-j"'	
fi

source /home/tenshin/.config/broot/launcher/bash/br
