set -g theme_color_scheme base16-light
if status --is-interactive
    set BASE16_SHELL "$HOME/.config/base16-shell/"
    source "$BASE16_SHELL/profile_helper.fish"
end

set fish_greeting

set theme_color_scheme terminal-light

#set fish_color_autosuggestion 888888
#set fish_color_command dddddd
#set fish_color_param aaaaaa
#set fish_color_operator 00aaff
#set fish_color_quote cccc55
#set fish_pager_color_prefix ff0000
#set fish_color_search_match --background=494949
#set fish_color_error ff0000

base16-classic-dark
