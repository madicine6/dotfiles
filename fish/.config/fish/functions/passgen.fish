function passgen
	cat /dev/urandom | tr -cd "[:alpha:][:digit:]" | tr -d "oO0lI" | head -c8
	echo
end
