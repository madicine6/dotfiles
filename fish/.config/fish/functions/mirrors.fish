function mirrors
	curl -s "https://www.archlinux.org/mirrorlist/?country=UA" > /tmp/mirrorlist
	chmod 755 /tmp/mirrorlist
	sed -i 's/#//' /tmp/mirrorlist
	rankmirrors -n 10 /tmp/mirrorlist | sudo tee /etc/pacman.d/mirrorlist
	rm /tmp/mirrorlist
end
